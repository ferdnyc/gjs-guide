---
title: Quick Settings
---

# Quick Settings

::: warning
This documentation is for GNOME 45 and later. Please see the
[Legacy Documentation][legacy-quicksettings] for previous versions.
:::

Quick settings is a user-interface pattern for the GNOME Shell [System Menu]. It
provides a simple, but flexible method for extensions to add indicators, toggles
and entry points for preferences to the System Menu.

[System Menu]: https://help.gnome.org/users/gnome-help/stable/shell-introduction.html#systemmenu
[legacy-quicksettings]: ../upgrading/legacy-documentation.md#quick-settings

## Imports

The following imports should be all most developers need for Quick Settings:

<<< @/../src/extensions/topics/quick-settings/extension.js#imports{js}


## Example Usage

This page will demonstrate a few simple examples of how quick settings can be
used in extensions. There are many complete examples of this UI pattern in GNOME
Shell, which can be referenced in the [`js/ui/status/`] directory.

[`js/ui/status/`]: https://gitlab.gnome.org/GNOME/gnome-shell/tree/main/js/ui/status

### System Indicator

::: tip
All extensions with quick settings should have an instance of this class, even
if they choose not to add an icon.
:::

The `QuickSettings.SystemIndicator` class is used to manage quick settings
items, and may also be used to display an icon:

<<< @/../src/extensions/topics/quick-settings/extension.js#example-indicator{js}


Add any quick settings items to the `quickSettingsItems` array, then call
`Panel.addExternalIndicator()` to have your indicator and items added to
an appropriate place:

<<< @/../src/extensions/topics/quick-settings/extension.js#example-extension{js}


### Basic Toggle

::: tip
See the [`NightLightToggle`] for an example in GNOME Shell.
:::

The most basic quick settings item is a simple toggle button. Examples in
GNOME Shell include *Dark Style*, *Night Light* and *Airplane Mode*.

<<< @/../src/extensions/topics/quick-settings/extension.js#example-toggle{js}


[`NightLightToggle`]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/status/nightLight.js

### Toggle Menu

::: tip
See the [`BluetoothToggle`] for an example in GNOME Shell.
:::

For features with a few more settings or options, you may want to add a submenu
to the toggle. The `QuickSettings.QuickMenuToggle` includes a built-in
[Popup Menu](popup-menu.md), that supports the standard menu functions:

<<< @/../src/extensions/topics/quick-settings/extension.js#example-menu-toggle{js}


[`BluetoothToggle`]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/status/bluetooth.js

### Slider

::: tip
See the [`BrightnessSlider`] for an example in GNOME Shell.
:::

The quick settings API also comes with a new class for sliders, for settings
like brightness or volume. The `QuickSettings.QuickSlider` class has a number
of features available, demonstrated below:

<<< @/../src/extensions/topics/quick-settings/extension.js#example-slider{js}


When adding the slider to the menu, you will usually want it to span two
columns, like the default volume slider:

```js
const myIndicator = new QuickSettings.SystemIndicator();
myIndicator.quickSettingsItems.push(new ExampleSlider());

Main.panel.statusArea.quickSettings.addExternalIndicator(myIndicator, 2);
```

[`BrightnessSlider`]: https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/status/brightness.js

### Action Button

::: warning
The action button area is a prominent location in the UI with limited space,
so consider carefully before adding more items.
:::

It's also possible to add action buttons to the top of the quick settings, such
as the *Lock Screen* or *Settings* button.

<<< @/../src/extensions/topics/quick-settings/extension.js#example-button{js}


Unlike other quick settings items, action buttons must be added manually:

```js
const QuickSettingsMenu = Main.panel.statusArea.quickSettings;
const QuickSettingsActions = QuickSettingsMenu._system._indicator.child;

QuickSettingsActions.add_child(new ExampleButton());
```
