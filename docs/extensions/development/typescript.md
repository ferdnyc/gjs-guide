---
title: TypeScript and LSP
---

# TypeScript and LSP

This page will guide you through creating an extension using TypeScript, which
will allow autocompletion to work in your editor. The setup presented here is
editor-agnostic and will work on any editor that support the Language Server
Protocol (LSP) or has some internal equivalent functionality.

## Creating the extension

Differently from what was previously done in the [Getting
Started](creating.html) section, the extension will now be created in a
directory outside the installation folder. This is mostly because GNOME Shell
does not support TypeScript, so we need a build phase to generate JavaScript
from our files, and then the generated files will be copied into the
installation folder.

To start, create a folder anywhere on your disk and, inside it, create the
following files:

### `metadata.json`

<<< @/../src/extensions/development/typescript/metadata.json{json}

This file does not contain anything different when compared to the one created
in the [Getting Started](creating.html#manual-creation), but is necessary for
the extension to work. Note that the specification has a `version` entry which
we do not specify, since it is generated automatically by E.G.O (GNOME's online
extension management system).

### `schemas/org.gnome.shell.extensions.my-extension.gschema.xml`

Attention: this file goes inside a `schemas` folder.

<<< @/../src/extensions/development/typescript/schemas/org.gnome.shell.extensions.my-extension.gschema.xml{xml}

This is a schema, as described in the [Preferences](preferences.html) section.
It is not necessary for an extension to work, but will be used in the example to
show how automate steps and how to generate the final zip for distribution.

## TypeScript setup

To use TypeScript we need some setup, installing some dependencies and
configuring the project to generate files correctly. In this example both the
`extensions.js` and `prefs.js` will be generated from their TypeScript
counterparts. Create the following files:

### `package.json`

<<< @/../src/extensions/development/typescript/package.json{json}

In this file, it is important to set `"type": "module"`. You can set `version`
to whatever you want, as it is not used.

Now that you have this file in place, you can run the following to install the
dependencies:

```sh:no-line-numbers
npm install --save-dev \
    eslint \
    eslint-plugin-jsdoc \
    typescript
npm install @girs/gjs @girs/gnome-shell
```

### `tsconfig.json`

<<< @/../src/extensions/development/typescript/tsconfig.json{json}

The TypeScript compiler configuration. Modifying it may break the build process.

### `ambient.d.ts`

<<< @/../src/extensions/development/typescript/ambient.d.ts{typescript}

This file makes it possible to use the usual import paths in your TypeScript
files instead of referencing `@girs/*` directly.

## The Extension and Preferences files

All the support files are in place, so we can finally write our extension's
code. Create the following files:

### `extension.ts`

<<< @/../src/extensions/development/typescript/extension.ts{typescript}

Thanks to `@girs`, the TypeScript code is basically what it would be if it were
JavaScript + type information. If you have been following the tutorial, your LSP
server should be able to offer information about the types in this file, like
auto-complete and go-to-definition.

This is also the very minimum necessary to have a working extension: a
default-exported class that extends `Extension` containing the methods
`enable()` and `disable()`. You should not create constructors/destructors, and
instead initialize your extension in `enable()` and finish it in `disable()`.
This is because your class my be constructed once and reused internally,
resulting in many calls to those methods.

### `prefs.ts`

<<< @/../src/extensions/development/typescript/prefs.ts{typescript}

This is also a good example of a minimal preference pane: a default-exported
class that extends `ExtensionPreferences` and implements
`fillPreferencesWindow(window: Adw.PreferencesWindow)`. There are other methods
that can be implemented instead, but this is the easiest to use. It also shows
how to populate the window with some widgets which are bound to properties
defined in the schema and therefore persisted.

## Build and packaging automation

Since we now have a build step, it is better to automate it. Create the
following file:

### `Makefile`

<<< @/../src/extensions/development/typescript/Makefile

You can now run `make` to compile your code and generate the files
`extension.js` and `prefs.js` inside the `dist` folder. If needed, it will
install the dependencies using `npm install`.

`make pack` will generate a file `my-extension.zip` which you can upload for
review. It will compile the code and the schema, if needed, and copy the
`schemas` folder and the `metadata.json` file into the `dest` folder before
zipping it.

`make install` will copy the files to the extensions folder. If you logout and
back in it should appear in the Extension Manager app.

Finally, `make clean` removes all generated files.
