---
title: Subprocesses
---

# Subprocesses

There are several ways to execute subprocesses with the GNOME platform, but
many of them are either cumbersome, error prone and most developers unknowingly
end up rewriting high-level APIs already available in Gio.

In contrast to the spawn functions available in GLib, `Gio.Subprocess` is both
simpler to use and safer for language bindings. It is just as powerful, does all
the cleanup you'd have to do yourself and is far more convenient for most use
cases.

#### Promise Wrappers

This document uses asynchronous methods wrapped with
[`Gio._promisify()`](../gjs/asynchronous-programming.md#promisify-helper).

::: details Copy & Paste
<<< @/../src/guides/gio/subprocesses/promisify.js{js}
:::

## Basic Usage

The simplest usage of `Gio.Subprocess` amounts to creating a new initialized
object. Once this function returns without error, the process will have started.

<<< @/../src/guides/gio/subprocesses/gioSubprocessBasic.js{js}

[initable]: https://gjs-docs.gnome.org/gio20/gio.initable

## Waiting for Processes

If you simply need to wait until a process completes before performing another
operation, the best choice is `Gio.Subprocess.wait_async()`. This will allow you
to maintain a sequence of operations without blocking the main loop:

<<< @/../src/guides/gio/subprocesses/gioSubprocessWait.js{js}

`Gio.Subprocess.wait_check_async()` is a convenience function for calling
`Gio.Subprocess.wait_async()` and then `Gio.Subprocess.get_successful()` in the
callback:

<<< @/../src/guides/gio/subprocesses/gioSubprocessWaitCheck.js{js}

[wait_check]: https://gjs-docs.gnome.org/gio20/gio.subprocess#method-wait_check

## Communicating with Processes

For single run processes with text output, the most convenient function is
[`Gio.Subprocess.communicate_utf8()`][communicate_utf8]. If the output of the
process is not text or you just want the output in `GLib.Bytes`, you can use
[`Gio.Subprocess.communicate()`][communicate] instead.

These two functions take (optional) input to pass to `stdin` and collect all the
output from `stdout` and `stderr`. Once the process completes the output is
returned.

<<< @/../src/guides/gio/subprocesses/gioSubprocessCommunicateUTF8.js{js}

For processes that continue to run in the background, you can queue a callback
for when the process completes while reading output and writing input as the
process runs.

Below is a contrived example using a simple shell script to read lines from
`stdin` and write them back to `stdout`:

<<< @/../src/guides/gio/subprocesses/gioSubprocessCommunicate.js{js}

[communicate]: https://gjs-docs.gnome.org/gio20/gio.subprocess#method-communicate
[communicate_utf8]: https://gjs-docs.gnome.org/gio20/gio.subprocess#method-communicate_utf8

## Extra Tips

There are a few extra tricks you can use when working with `Gio.Subprocess`.

### Cancellable Processes

`Gio.Subprocess` implements the [`Gio.Initable`][initable] interface, which
allows for failable initialization. You may find passing a cancellable useful to
prevent the process from starting if already cancelled, or connecting to it to
call [`Gio.Subprocess.force_exit()`][force_exit] if triggered later:

<<< @/../src/guides/gio/subprocesses/gioSubprocessExecCancellable.js{js}

### Command Line Parsing

If you happen to have the command line as a single string, you can use the
[`GLib.shell_parse_argv()`][shell_parse_argv] function to parse it as a list of
strings to pass to `Gio.Subprocess`. This function can handle most common shell
quoting, but may fail on some more complex usage.

<<< @/../src/guides/gio/subprocesses/glibShellParseArgv.js{js}

[shell_parse_argv]: https://gjs-docs.gnome.org/glib20/glib.shell_parse_argv

## GSubprocessLauncher

[`Gio.SubprocessLauncher`][gsubprocesslauncher] is a re-usable object you can
use to spawn processes. You can set the flags at construction, then just call
`Gio.SubprocessLauncher.spawnv()` with your arguments any time you want to
spawn a process.

It also allows you to designate files for input and output, change the working
directory and set or modify environment variables, which is especially useful
for spawning shell scripts.

In every other way, the returned object is a regular `Gio.Subprocess` object
and you can still call methods like [`communicate_utf8()`][communicate_utf8],
[`wait_check()`][wait_check] and [`force_exit()`][force_exit] on it.

<<< @/../src/guides/gio/subprocesses/gioSubprocessLauncher.js{js}

[gsubprocesslauncher]: https://gjs-docs.gnome.org/gio20/gio.subprocesslauncher
[force_exit]: https://gjs-docs.gnome.org/gio20/gio.subprocess#method-force_exit


## Complete Examples

Below is a few more complete, Promise-wrapped functions you can use in your
code. The advantages here over `GLib.spawn_command_line_async()` are checking
the process actually completes successfully, the ability to stop it at any time,
and notification when it does or improved errors when it doesn't.

<<< @/../src/guides/gio/subprocesses/gioSubprocessHelpers.js{js}
