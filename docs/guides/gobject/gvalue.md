---
title: GValue
---

# GValue

[`GObject.Value`][gvalue] is a generic value container, usually only used to
implement [GObject Properties](basics.md#properties) in projects written with
the C programming language. By storing the value type alongside the value, it
allows for dynamic type features usually not available to C programmers.

In JavaScript, this behavior is part of the language (i.e. `typeof`) and GJS
will usually convert them automatically, but there are some situations that
require using `GObject.Value` directly.

[gvalue]: https://gjs-docs.gnome.org/gobject20/gobject.value

## Basic Usage

Before a newly created GValue can be used, it must be initialized to hold a
specific [GType](gtype.md#type-constants):

<<< @/../src/guides/gobject/gvalue/basic.js#gvalue-init{js}

The value can then be set directly, or passed to a function that takes it as an
argument and sets the value.

<<< @/../src/guides/gobject/gvalue/basic.js#gvalue-set{js}

The type of an initialized `GObject.Value` can be checked by calling
[`GObject.type_check_value_holds()`]:

<<< @/../src/guides/gobject/gvalue/basic.js#gvalue-check{js}

[`GObject.type_check_value_holds()`]: https://gjs-docs.gnome.org/gobject20/gobject.type_check_value_holds

## GObject Properties

Although you should always use JavaScript property accessors for native values,
the `GObject.Object.get_property()` and `GObject.Object.set_property()` methods
can be used to work with a `GObject.Value` that will be passed to another
function.

<<< @/../src/guides/gobject/gvalue/properties.js#gvalue-properties{js}

## Return Values and Callback Arguments

There are situations where a function may expect a particular value type (e.g.
`GObject.TYPE_INT64`), but GJS can not determine this from the incoming type
(e.g. `Number`). However, in most cases when `GObject.Value` is returned from
functions or passed as callback arguments, they will be automatically unpacked.

Below is a non-functional example of Drag-n-Drop, where the `GObject.Value` is
automatically unpacked for the `Gtk.DropTarget::drop` signal:

<<< @/../src/guides/gobject/gvalue/other.js#gvalue-return-args{js}

