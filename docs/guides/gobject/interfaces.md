---
title: Interfaces
---

# GObject Interfaces

In plain JavaScript interfaces are usually informal and simply fulfilled by the
presence of certain methods and properties on an object. However, you may
recognize interfaces from TypeScript, where an object's type includes
information about its capabilities. GObject interfaces are a way of ensuring
that objects passed to C code have the right capabilities.

For example, the [`Gio.Icon`] interface is implemented by [`Gio.FileIcon`] for
file-based icons and [`Gio.ThemedIcon`] for themed icons. Instances of these
classes, or a custom JavaScript implementation, can be passed to [`Gtk.Image`].

[`Gio.Icon`]: https://gjs-docs.gnome.org/gio20/gio.icon
[`Gio.FileIcon`]: https://gjs-docs.gnome.org/gio20/gio.fileicon
[`Gio.ThemedIcon`]: https://gjs-docs.gnome.org/gio20/gio.themedicon
[`Gtk.Image`]: https://gjs-docs.gnome.org/gtk40/gtk.image

## Implementing Interfaces

Implementing an interface involves providing working implementations for class
methods and properties defined by the interface.

### Methods

Interfaces that require methods to be implemented must have the corresponding
virtual function defined in the class. For example, the virtual function for
the `Gio.ListModel` method `get_item()` is `vfunc_get_item()`. When a caller
invokes `Gio.ListModel.get_item()` on an object it will defer to the virtual
function of the implementation.

This is different from overriding a function in native JavaScript classes and
interfaces, where the method should be overridden using the original member
name.

Below is an example implementation of the [`Gio.ListModel`] interface which
only requires implementing three methods:

<<< @/../src/guides/gobject/interfaces/implementation.js#array-store{js}

[glistmodel]: https://gjs-docs.gnome.org/gio20/gio.listmodel

### Properties

Interfaces that require properties to be implemented must have the GParamSpec
overridden in the class registration, as well as the JavaScript getter and/or
setter implemented.

Below is an example of implementing the [`GtkOrientable`] interface from GTK,
which only requires implementing one property. The `orientation` property is a
read-write property, so we implement both `get` and `set` functions and register
it in the properties dictionary.

<<< @/../src/guides/gobject/interfaces/implementation.js#orientable-object{js}


[`GtkOrientable`]: https://gjs-docs.gnome.org/gtk40/gtk.orientable

### Multiple Interfaces

It is also possible for a class to implement multiple interfaces. The example
below is an incomplete example of a container widget implementing both
`Gtk.Orientable` and `Gio.ListModel`:

<<< @/../src/guides/gobject/interfaces/implementation.js#orientable-widget{js}


## Defining Interfaces

::: tip
GObject Interfaces exist to implement type safe multiple-inheritance in the C
programming language, while JavaScript code should usually just use mix-ins.
:::

Interfaces are defined in GJS by inheriting from `GObject.Interface` and
providing the class definition property `Requires`. This field must include a
base type that is `GObject.Object` or a subclass of `GObject.Object`.

The `Requires` field may also contain multiple other interfaces that are either
implemented by the base type, or that the implementation is expected to. For
example, `Requires: [GObject.Object, Gio.Action]` indicates that an
implementation must provide methods, properties and emit signals from the
[`Gio.Action`] interface, or be derived from a base type that does.

#### Defining Methods

Methods defined on an interface must be implemented, if the method throws the
special error `GObject.NotImplementedError()`. Methods that do not throw this
error are optional to implement.

Note that unlike GObject Interfaces defined by a C library, methods are
overridden directly rather than by virtual function. For example, instead of
overriding `vfunc_requiredMethod()`, you should override `requiredMethod()`.

#### Defining Properties

Properties defined on an interface must always be implemented, using
`GObject.ParamSpec.override()` in the `Properties` class definition property.
The implementation should also provide `get` and `set` methods for the
property, as indicated by the [GObject Property Flags].

[GObject Property Flags]: subclassing.html#property-flags

#### Defining Signals

Signals defined on an interface do not need to be implemented. Typically
interface definitions will provide emitter methods, such as with
[`Gio.ListModel.items_changed()`], otherwise they can be emitted by calling
[`GObject.Object.prototype.emit()`] on an instance of the implementation.

### A Simple Interface

Below is a simple example of defining an interface that only requires
`GObject.Object`:

<<< @/../src/guides/gobject/interfaces/definition.js#simple-interface{js}


Note that unlike with interfaces defined by C libraries, we override methods
like `requiredMethod()` directly, not `vfunc_requiredMethod()`. Below is a
minimal implementation of `SimpleInterface`:

<<< @/../src/guides/gobject/interfaces/definition.js#simple-implementation{js}


Instances of the implementation can then be constructed like any class. The
`instanceof` operator can be used to confirm the base class (i.e. `GObject`) and
any interfaces it implements:

<<< @/../src/guides/gobject/interfaces/definition.js#simple-instance{js}


### A Complex Interface

More complex interfaces can also be defined that depend on other interfaces,
including those defined in GJS. `ComplexInterface` depends on `Gio.ListModel`
and `SimpleInterface`, while adding a property and a method.

<<< @/../src/guides/gobject/interfaces/definition.js#complex-interface{js}


An implementation of this interface must then meet the requirements of
`Gio.ListModel` and `SimpleInterface`, which both require `GObject.Object`. The
following implementation of `ComplexInterface` will meet the requirements of:

* [`GObject.Object`] and [`Gio.ListModel`] by inheriting from [`Gio.ListStore`]
* `SimpleInterface` by implementing its methods and properties
* `ComplexInterface` by implementing its methods and properties

<<< @/../src/guides/gobject/interfaces/definition.js#complex-implementation{js}


By using `instanceof`, we can confirm both the inheritance and interface support
of the implementation:

<<< @/../src/guides/gobject/interfaces/definition.js#complex-instance{js}



[`GObject.Object`]: https://gjs-docs.gnome.org/gobject20/gobject.object
[`GObject.Object.prototype.emit()`]: https://gjs-docs.gnome.org/gjs/overrides.md#gobject-object-emit
[`Gio.Action`]: https://gjs-docs.gnome.org/gio20/gio.action
[`Gio.ListModel`]: https://gjs-docs.gnome.org/gio20/gio.listmodel
[`Gio.ListModel.get_item()`]: https://gjs-docs.gnome.org/gio20/gio.listmodel#method-get_item
[`Gio.ListModel.items_changed()`]: https://gjs-docs.gnome.org/gio20/gio.listmodel#method-items_changed
[`Gio.ListStore`]: https://gjs-docs.gnome.org/gio20/gio.liststore
