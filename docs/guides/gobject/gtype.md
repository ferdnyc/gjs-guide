---
title: GType
---

# GType

GType is the foundation of the GObject system. Although it is rarely necessary
to interact with a GType directly in GJS, there are some situations where you
may need to pass a GType as a function argument or in a class definition.

## GType Object

Every GObject class has a static `$gtype` property that holds a GType object for
the given type. This is the proper way to find the GType for an object class or
instance. If an instance of the class has been constructed, you could also call
[`GObject.type_from_name()`].

<<< @/../src/guides/gobject/gtype/object.js#gtype-from-name{js}

To check the GType of a class instance, you can use the [`instanceof`] operator
to compare it to a constructor object. This will return `true` for any parent
type, including interfaces like `Gio.Icon`.

<<< @/../src/guides/gobject/gtype/object.js#gtype-instanceof{js}

In many cases, you can pass the constructor object instead of a GType, which is
often more convenient and idiomatic in JavaScript.

<<< @/../src/guides/gobject/gtype/object.js#gtype-from-constructor{js}

[`GObject.type_from_name()`]: https://gjs-docs.gnome.org/gobject20/gobject.type_from_name
[`constructor`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/constructor
[`instanceof`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof

## GType Name

The `name` property of a GType object gives the GType name as a string (the same
name passed to [`GObject.type_from_name()`]). This is the proper way to find the
type name for an object class or instance. By default, the GType name of a
subclass in GJS will be the JavaScript class name prefixed with `Gjs_`:

<<< @/../src/guides/gobject/gtype/name.js#gtype-name{js}

In most cases you will not need to specify your own name, however, this may be
useful when creating a GtkBuilder template class. To set the GType name, pass
it as the value for the `GTypeName` property to `GObject.registerClass()`.

```xml:no-line-numbers
<interface>
  <template class="Square" parent="GtkBox">
    <!-- Template Definition -->
  </template>
</interface>
```

<<< @/../src/guides/gobject/gtype/name.js#gtype-gtkbuilder{js}

## JavaScript Types

::: warning
`GObject.TYPE_JSOBJECT` is a boxed type, so it may not be used where a GObject
is expected, such as with [`Gio.ListModel`].
:::

`GObject.TYPE_JSOBJECT` is a special `GType` in GJS, created so that JavaScript
types that inherit from [`Object`] can be used with the GObject framework. This
allows you to use them as [property types](subclassing.md#property-types) and
in [signal parameters](subclassing.md#signal-parameters) in your GObject
subclasses.

This means that you may use `Object` and `Array` types, but also more complex
types such as `Date` and `Function`. However, it will not allow you to use
JavaScript [primitive data types] like `BigInt` or `Symbol`.

<<< @/../src/guides/gobject/gtype/constants.js#gtype-jsobject{3-8,11-13 js:no-line-numbers}

[`Object`]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object
[`Gio.ListModel`]: https://gjs-docs.gnome.org/gio20/gio.listmodel
[primitive data types]: https://developer.mozilla.org/docs/Glossary/Primitive

## Type Constants

For convenience, GJS has predefined constants for most built-in types.

| Constant                 | GLib             | JavaScript          |
|--------------------------|------------------|---------------------|
| `GObject.TYPE_BOOLEAN`   | `gboolean`       | `Boolean`           |
| `GObject.TYPE_STRING`    | `gchararray`     | `String`            |
| `GObject.TYPE_INT`       | `gint`           | `Number`            |
| `GObject.TYPE_UINT`      | `guint`          | `Number`            |
| `GObject.TYPE_LONG`      | `glong`          | `Number`            |
| `GObject.TYPE_ULONG`     | `gulong`         | `Number`            |
| `GObject.TYPE_INT64`     | `gint64`         | `Number`            |
| `GObject.TYPE_UINT64`    | `guint64`        | `Number`            |
| `GObject.TYPE_FLOAT`     | `gfloat`         | `Number`            |
| `GObject.TYPE_DOUBLE`    | `gdouble`        | `Number`            |
| `GObject.TYPE_ENUM`      | `GEnum`          | `Number`            |
| `GObject.TYPE_FLAGS`     | `GFlags`         | `Number`            |
| `GObject.TYPE_OBJECT`    | `GObject`        | `GObject.Object`    |
| `GObject.TYPE_INTERFACE` | `GInterface`     | `GObject.Interface` |
| `GObject.TYPE_BOXED`     | `GBoxed`         |                     |
| `GObject.TYPE_POINTER`   | `gpointer`       | nothing             |
| `GObject.TYPE_PARAM`     | `GParam`         | `GObject.ParamSpec` |
| `GObject.TYPE_VARIANT`   | `GVariant`       | `GLib.Variant`      |
| `GObject.TYPE_GTYPE`     | `GType`          | `GObject.Type`      |
| `GObject.TYPE_JSOBJECT`  | `GBoxed`         | `Object`            |

