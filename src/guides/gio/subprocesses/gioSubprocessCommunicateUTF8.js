import Gio from 'gi://Gio';


try {
    const proc = Gio.Subprocess.new(['ls', '/'],
        Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE);

    const [stdout, stderr] = await proc.communicate_utf8_async(null, null);

    if (proc.get_successful())
        console.log(stdout);
    else
        throw new Error(stderr);
} catch (e) {
    logError(e);
}
