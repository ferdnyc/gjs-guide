import GLib from 'gi://GLib';


// This function may throw an error
try {
    // Returns: ['ls', '-l', '/']
    const [, argv1] = GLib.shell_parse_argv('ls -l /');

    // Returns: ['ls', '-l', '/dir with spaces']
    const [, argv2] = GLib.shell_parse_argv('ls -l "/dir with spaces"');
} catch (e) {
    logError(e);
}
