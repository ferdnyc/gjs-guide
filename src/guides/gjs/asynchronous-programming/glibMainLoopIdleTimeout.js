import GLib from 'gi://GLib';


const loop = new GLib.MainLoop(null, false);

// Timeout sources execute a callback when the interval is reached
const timeoutId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 1, () => {
    console.log('This callback was invoked because the timeout was reached');

    return GLib.SOURCE_REMOVE;
});


// Idle sources execute a callback when no other sources with a higher priority
// are ready.
const idleId = GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
    console.log('This callback was invoked because no other sources were ready');

    return GLib.SOURCE_REMOVE;
});

await loop.runAsync();
