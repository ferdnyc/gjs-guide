import GLib from 'gi://GLib';
import Gio from 'gi://Gio';


const file = Gio.File.new_for_path('test-file.txt');

// Here is the synchronous, blocking form of this operation
try {
    const [, contents] = file.load_contents(null);

    console.log(`Read ${contents.length} bytes from ${file.get_basename()}`);
} catch (e) {
    logError(e, `Reading ${file.get_basename()}`);
}

// Here is an asynchronous, non-blocking wrapper in use
try {
    const [, contents] = await new Promise((resolve, reject) => {
        file.load_contents_async(GLib.PRIORITY_DEFAULT, null, (_file, res) => {
            try {
                // If the task succeeds, we can return the result with resolve()
                resolve(file.load_contents_finish(res));
            } catch (e) {
                // If an error occurred, we can report it using reject()
                reject(e);
            }
        });
    });

    console.log(`Read ${contents.length} bytes from ${file.get_basename()}`);
} catch (e) {
    logError(e, `Reading ${file.get_basename()}`);
}
