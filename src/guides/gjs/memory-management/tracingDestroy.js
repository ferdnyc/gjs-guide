import St from 'gi://St';


let myLabel = new St.Label({
    text: 'Some Text',
});

// Here we are FORCING the GObject to be freed, as though it's reference
// count had dropped to 0.
myLabel.destroy();

// Even though this GObject is being traced from `myLabel`, trying to call
// its methods or access its properties will raise a critical error because
// all it's resources have been freed.
console.log(myLabel.text);

// In this case we are in the top-level scope, so the proper thing to do is
// `null` the variable to allow garbage collector to free the JS wrapper.
myLabel = null;
