import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk?version=4.0';


// #region array-store
const ArrayStore = GObject.registerClass({
    Implements: [Gio.ListModel],
}, class ArrayStore extends GObject.Object {
    #items = [];

    vfunc_get_item(position) {
        return this.#items[position] || null;
    }

    vfunc_get_item_type() {
        return GObject.Object;
    }

    vfunc_get_n_items() {
        return this.#items.length;
    }

    /**
     * Insert an item in the list. If @position is greater than the number of
     * items in the list or less than `0` it will be appended to the end of the
     * list.
     *
     * @param {GObject.Object} item - the item to add
     * @param {number} [position] - the position to add the item
     */
    insertItem(item, position = -1) {
        // Type check the item
        if (!(item instanceof GObject.Object))
            throw TypeError(`Not a GObject: ${item.constructor.name}`);

        if (!GObject.type_is_a(item.constructor.$gtype, this.get_item_type()))
            throw TypeError(`Invalid type: ${item.constructor.$gtype.name}`);

        // Normalize the position
        if (position < 0 || position > this.#items.length)
            position = this.#items.length;

        // Insert the item, then emit Gio.ListModel::items-changed
        this.#items.splice(position, 0, item);
        this.items_changed(position, 0, 1);
    }

    /**
     * Remove the item at @position. If @position is outside the length of the
     * list, this function does nothing.
     *
     * @param {number} position - the position of the item to remove
     */
    removeItem(position) {
        // NOTE: The Gio.ListModel interface will ensure @position is an
        //       unsigned integer, but other methods must check explicitly.
        if (position < 0 || position >= this.#items.length)
            return;

        // Remove the item and emit Gio.ListModel::items-changed
        this.#items.splice(position, 1);
        this.items_changed(position, 1, 0);
    }
});
// #endregion array-store


// #region orientable-object
const OrientableObject = GObject.registerClass({
    Implements: [Gtk.Orientable],
    Properties: {
        'orientation': GObject.ParamSpec.override('orientation',
            Gtk.Orientable),
    },
}, class OrientableObject extends GObject.Object {
    get orientation() {
        if (this._orientation === undefined)
            this._orientation = Gtk.Orientation.HORIZONTAL;

        return this._orientation;
    }

    set orientation(value) {
        if (this.orientation === value)
            return;

        this._orientation = value;
        this.notify('orientation');
    }
});
// #endregion orientable-object


// #region orientable-widget
const OrientableWidget = GObject.registerClass({
    Implements: [Gio.ListModel, Gtk.Orientable],
    Properties: {
        'orientation': GObject.ParamSpec.override('orientation',
            Gtk.Orientable),
    },
}, class OrientableWidget extends Gtk.Widget {
    constructor(params = {}) {
        super(params);

        this._children = [];
    }

    get orientation() {
        if (this._orientation === undefined)
            this._orientation = Gtk.Orientation.HORIZONTAL;

        return this._orientation;
    }

    set orientation(value) {
        if (this.orientation === value)
            return;

        this._orientation = value;
        this.notify('orientation');
    }

    vfunc_get_item_type() {
        return Gtk.Widget;
    }

    vfunc_get_item(position) {
        return this._children[position] || null;
    }

    vfunc_get_n_items() {
        return this._children.length;
    }
});
// #endregion orientable-widget
