import GObject from 'gi://GObject';
import Gio from 'gi://Gio';

// #region gvalue-properties
const action = new Gio.SimpleAction({
    name: 'test',
    enabled: false,
});

// Create a new boolean GValue
const booleanValue = new GObject.Value();
booleanValue.init(GObject.TYPE_BOOLEAN);

// Get the GValue for a GObject property
action.get_property('enabled', booleanValue);
console.log(booleanValue.get_boolean());

// Set a GObject property from a GValue
booleanValue.set_boolean(true);
action.set_property('enabled', booleanValue);
// #endregion gvalue-properties
