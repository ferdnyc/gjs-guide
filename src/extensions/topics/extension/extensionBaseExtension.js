import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';


let extensionObject, extensionSettings;

// Getting the extension object by UUID
extensionObject = Extension.lookupByUUID('example@gjs.guide');
extensionSettings = extensionObject.getSettings();
console.log(extensionObject.metadata);

// Getting the extension object by URL
extensionObject = Extension.lookupByURL(import.meta.url);
extensionSettings = extensionObject.getSettings();
console.log(extensionObject.metadata);
