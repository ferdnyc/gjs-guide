import type { DefaultTheme } from 'vitepress/types/default-theme';

/**
 *
 * Guides sidebar
 *
 */

/**
 * GJS section
 */

const gjs = {
  text: 'GJS',
  collapsed: false,
  items: [
    { text: 'Intro', link: '/guides/gjs/intro.md' },
    {
      text: 'Asynchronous Programming',
      link: '/guides/gjs/asynchronous-programming.md',
    },
    { text: 'Style Guide', link: '/guides/gjs/style-guide.md' },
    { text: 'Memory Management', link: '/guides/gjs/memory-management.md' },
  ],
} satisfies DefaultTheme.SidebarItem;

/**
 * GLib, GObject & Gio sections
 */

const glib = {
  text: 'GLib',
  collapsed: false,
  items: [{ text: 'GVariant', link: '/guides/glib/gvariant.md' }],
} satisfies DefaultTheme.SidebarItem;

const gobject = {
  text: 'GObject',
  collapsed: false,
  items: [
    { text: 'Basics', link: '/guides/gobject/basics.md' },
    { text: 'Interfaces', link: '/guides/gobject/interfaces.md' },
    { text: 'Subclassing', link: '/guides/gobject/subclassing.md' },
    { text: 'GType', link: '/guides/gobject/gtype.md' },
    { text: 'GValue', link: '/guides/gobject/gvalue.md' },
    { text: 'Advanced', link: '/guides/gobject/advanced.md' },
  ],
} satisfies DefaultTheme.SidebarItem;

const gio = {
  text: 'Gio',
  collapsed: false,
  items: [
    { text: 'Actions and Menus', link: '/guides/gio/actions-and-menus.md' },
    { text: 'DBus', link: '/guides/gio/dbus.md' },
    { text: 'File Operations', link: '/guides/gio/file-operations.md' },
    { text: 'List Models', link: '/guides/gio/list-models.md' },
    { text: 'Subprocesses', link: '/guides/gio/subprocesses.md' },
  ],
} satisfies DefaultTheme.SidebarItem;

/**
 * GTK4 sub-section
 */

const gtk4 = {
  text: 'GTK4',
  items: [
    {
      text: 'Gnome Developer Getting Started',
      link: 'https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html',
    },
    {
      text: 'GTK4 Book',
      link: 'https://rmnvgr.gitlab.io/gtk4-gjs-book/',
    },
  ],
} satisfies DefaultTheme.SidebarItem;

/**
 * GTK3 sub-section
 */

const gtk3 = {
  text: 'GTK3',
  items: [
    { text: 'Intro', link: '/guides/gtk/3/README.md' },
    { text: 'Basics', link: '/guides/gtk/3/01-basics.md' },
    { text: 'Widgets', link: '/guides/gtk/3/02-widgets.md' },
    { text: 'Installing', link: '/guides/gtk/3/03-installing.md' },
    { text: 'Running GTK', link: '/guides/gtk/3/04-running-gtk.md' },
    { text: 'Layouts', link: '/guides/gtk/3/05-layouts.md' },
    { text: 'Text', link: '/guides/gtk/3/06-text.md' },
    { text: 'Buttons', link: '/guides/gtk/3/07-buttons.md' },
    { text: 'Editing Text', link: '/guides/gtk/3/08-editing-text.md' },
    { text: 'Images', link: '/guides/gtk/3/09-images.md' },
    { text: 'Building App', link: '/guides/gtk/3/10-building-app.md' },
    { text: 'Packaging', link: '/guides/gtk/3/11-packaging.md' },
    { text: 'App Dev', link: '/guides/gtk/3/12-app-dev.md' },
    { text: 'UI', link: '/guides/gtk/3/13-ui.md' },
    { text: 'Templates', link: '/guides/gtk/3/14-templates.md' },
    { text: 'Saving Data', link: '/guides/gtk/3/15-saving-data.md' },
    { text: 'Settings', link: '/guides/gtk/3/16-settings.md' },
    { text: 'Dialogs', link: '/guides/gtk/3/17-dialogs.md' },
  ],
} satisfies DefaultTheme.SidebarItem;

/**
 * Application packaging sub-section
 */

const applicationPackaging = {
  text: 'GTK application packaging',
  items: [
    {
      text: 'Application Packaging',
      link: '/guides/gtk/application-packaging.md',
    },
  ],
} satisfies DefaultTheme.SidebarItem;

/**
 * GTK section
 */

const gtk = {
  text: 'GTK',
  collapsed: false,
  items: [{ ...gtk4 }, { ...gtk3 }, { ...applicationPackaging }],
} satisfies DefaultTheme.SidebarItem;

/**
 *
 * Extensions sidebar
 *
 */

/**
 * Development section
 */

const development = {
  text: 'Development',
  collapsed: false,

  items: [
    { text: 'Getting Started', link: '/extensions/development/creating.md' },
    { text: 'Translations', link: '/extensions/development/translations.md' },
    { text: 'Preferences', link: '/extensions/development/preferences.md' },
    { text: 'Accessibility', link: '/extensions/development/accessibility.md' },
    { text: 'Debugging', link: '/extensions/development/debugging.md' },
    {
      text: 'Targeting Older GNOME',
      link: '/extensions/development/targeting-older-gnome.md',
    },
    {
      text: 'TypeScript and LSP',
      link: '/extensions/development/typescript.md',
    },
  ],
} satisfies DefaultTheme.SidebarItem;

/**
 * Overview section
 */

const overview = {
  text: 'Overview',
  collapsed: false,

  items: [
    { text: 'Anatomy', link: '/extensions/overview/anatomy.md' },
    { text: 'Architecture', link: '/extensions/overview/architecture.md' },
    {
      text: 'Imports And Modules',
      link: '/extensions/overview/imports-and-modules.md',
    },
    {
      text: 'Updates And Breakage',
      link: '/extensions/overview/updates-and-breakage.md',
    },
  ],
} satisfies DefaultTheme.SidebarItem;

/**
 * Topics section
 */
const topics = {
  text: 'Topics',
  collapsed: false,

  items: [
    { text: 'Extension', link: '/extensions/topics/extension.md' },
    { text: 'Extension Utils', link: '/extensions/topics/extension-utils.md' },
    { text: 'Dialogs', link: '/extensions/topics/dialogs.md' },
    { text: 'Notifications', link: '/extensions/topics/notifications.md' },
    { text: 'Popup Menu', link: '/extensions/topics/popup-menu.md' },
    { text: 'Quick Settings', link: '/extensions/topics/quick-settings.md' },
    { text: 'Search Provider', link: '/extensions/topics/search-provider.md' },
    { text: 'Session Modes', link: '/extensions/topics/session-modes.md' },
  ],
} satisfies DefaultTheme.SidebarItem;

/**
 * Upgrading section
 */

const upgrading = {
  text: 'Upgrading',
  collapsed: false,

  items: [
    { text: 'GNOME Shell 47', link: '/extensions/upgrading/gnome-shell-47.md' },
    { text: 'GNOME Shell 46', link: '/extensions/upgrading/gnome-shell-46.md' },
    { text: 'GNOME Shell 45', link: '/extensions/upgrading/gnome-shell-45.md' },
    { text: 'GNOME Shell 44', link: '/extensions/upgrading/gnome-shell-44.md' },
    { text: 'GNOME Shell 43', link: '/extensions/upgrading/gnome-shell-43.md' },
    { text: 'GNOME Shell 42', link: '/extensions/upgrading/gnome-shell-42.md' },
    { text: 'GNOME Shell 40', link: '/extensions/upgrading/gnome-shell-40.md' },
    {
      text: 'Legacy Documentation',
      link: '/extensions/upgrading/legacy-documentation.md',
    },
  ],
} satisfies DefaultTheme.SidebarItem;

/**
 * Review guidelines section
 */

const review = {
  text: 'Review Guidelines',
  collapsed: false,

  items: [
    {
      text: 'Review Guidelines',
      link: '/extensions/review-guidelines/review-guidelines.md',
    },
  ],
};

/**
 * Sidebar
 */

const sidebar = {
  '/guides/': [{ ...gjs }, { ...glib }, { ...gobject }, { ...gio }, { ...gtk }],

  '/extensions/': [
    { ...development },
    { ...overview },
    { ...topics },
    { ...upgrading },
    { ...review },
  ],
} satisfies DefaultTheme.Sidebar;

export default sidebar;
